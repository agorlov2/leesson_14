﻿// Lesson_14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

using namespace std;

int main()

{
	setlocale(LC_ALL, "ru");
	string String = "I study Unreal Engine 4";

	cout << "Строковая переменная String  " << String << endl;
	cout << "Длина строки  " << String.length() << endl;
	cout << "Первый символ строки  " << String.substr(0, 1) << endl;
	cout << "Последний символ строки  " << String.substr(String.length() - 1, 1) << endl;



	return 0;

}